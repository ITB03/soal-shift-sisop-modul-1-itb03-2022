#!/bin/bash

users_file=/home/dimas/users/user.txt
log=/home/dimas/log.txt
read -p "username: " username
read -p "password: " -s password

timestamp(){
	date +"%D %T"
}

times=$(timestamp)

ambil=$(awk -v user="${username}" -v pass="${password}" '$1==user && $3==pass {print 1}' $users_file)
if [[ ! "$ambil" == 1 ]];
then
	echo "$times LOGIN: ERROR Failed login attempt on user $username" >> $log
	read -p "username: " username
	read -p "password: " -s password
	ambil=$(awk -v user="${username}" -v pass="${password}" '$1==user && $2==pass {print 1}' $users_file)
fi

echo "$times LOGIN: INFO User $username logged in" >> $log

read -p "att / dl (N): " input
if [[ "$input" == "att" ]];
	then
			awk -v user="${username}" '$3=="LOGIN:" && $6==user {++n} END {print "Login attempt = " n}' $log
else
	sudo apt install zip unzip
	folder=`date +%F`"_$username"
	index=0
	if [[ ! -f "./$folder.zip" ]]
	then
		mkdir -p "./$folder"
	else
		unzip -P "$password" "./$folder.zip"
		file=`ls "$folder" -r | head -n 1`
		index=$(awk -F"_"'{print $2}' <<< "$file")
	fi
	for ((num=index+1; num<=index+cc; num++))
	do
		wget -O "$folder/PIC_$num" https://loremflickr.com/320/240
	done
	zip -r -P "$password" "$folder.zip" "$folder"
	rm -rf "$folder"
fi