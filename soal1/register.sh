#!bin/bash

home=/home/dimas/users
users_file=$home/user.txt
log=/home/dimas/log.txt

if [[ ! -d "$home" ]]
then
	mkdir $home
fi

timestamp() {
date +"%D %T"
}

times=$(timestamp)

read -p "Masukkan username: " username

until ! grep -q -s "$username" $users_file;
do
	echo "username sudah ada"
	read -p "Masukkan username: " username
	echo "$times REGISTER: ERROR User already exists" >> $log
done

echo "$times REGISTER: INFO User $username logged in" >> $log

read -p "Masukkan password: " -s password
echo -e

if [[ ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && ! "$password" == "$username" ]];
then
	echo "tidak memenuhi syarat"
	read -p "Masukkan password: " -s password
	echo -e
fi

echo "$username ; $password" >> $users_file