mkdir -p ./forensic_log_website_daffainfo_log
test -f ./forensic_log_website_daffainfo_log/ratarata.txt || touch ./forensic_log_website_daffainfo_log/ratarata.txt
test -f ./forensic_log_website_daffainfo_log/result.txt || touch ./forensic_log_website_daffainfo_log/result.txt

#2B
avg(){
msg="$1"
avg_msg="$msg"
echo "$avg_msg" >> forensic_log_website_daffainfo_log/ratarata.txt 
}

cat forensic_log_website_daffainfo_log/log_website_daffainfo.log | awk -F ":" '{sum+=$3$4$5} END {rata_rata = sum/NR}'
msg="Rata-rata serangan adalah sebanyak $rata_rata requests per jam"
avg "$msg"

#2D
jumlah_req_curl=$(awk 'BEGIN {count=0} $NF ~ /curl/ {count++} END {print count}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
echo "Ada $jumlah_req_curl requests yang menggunakan curl sebagai user-agent">> forensic_log_website_daffainfo_log/result.txt

#22/Jan/2022:02
#2E
n=$(awk 'END{print NR}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
for i in n
do
IP_Address_Jam_2_pagi=$(awk -F ":" '$3==02 {print $1}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
echo "$IP_Address_Jam_2_pagi" >> forensic_log_website_daffainfo_log/result.txt
done
