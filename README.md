# README

# Soal Nomor 1

| Pengerjaan | NRP |
| --- | --- |
| Dimas Bagus R | 5027201034 |
1. Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.
    1. Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script [main.sh](http://main.sh/)
    2. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
        1. Minimal 8 karakter
        2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
        3. Alphanumeric
        4. Tidak boleh sama dengan username
    3. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss **MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.
        1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
        2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User **USERNAME** registered successfully
        3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user **USERNAME**
        4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User **USERNAME** logged in
    4. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
        1. dl N ( N = Jumlah gambar yang akan didownload)

Untuk mendownload gambar dari [https://loremflickr.com/320/240](https://loremflickr.com/320/240) dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD**_**USERNAME**. Gambar-gambar yang didownload juga memiliki format nama **PIC_XX**, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.

ii. att

Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

## Solusi

a.

Han membuat sistem register dan login terlebih dahulu. Sistem register dan login dibuat dengan script [register.sh](http://register.sh) dan [main.sh](http://main.sh) dengan syarat-syarat sebagai berikut:

- Minimal 8 karakter
- Memiliki minimal satu huruf kapital dan satu huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

Setiap user yang didaftarkan akan disimpan dalam /users/user.txt. Command yang digunakan untuk membuat direktori yang diinginkan adalah: 

```bash
mkdir -p ./users
test -f ./users/user.txt || touch ./users/user.txt
test -f log.txt || touch log.txt
```

Kemudia sistem register dan login dibuat dengan beberapa syarat keamanan.  Input username dan password menggunakan command:

```bash
read -p "Masukkan username: " username
read -p "Masukkan password: " -s password
echo -e
```

User yang berhasil didaftarkan disimpan dalam file users/user.txt dengan command:

```bash
echo "username: $username ; password: $password" >> .users/user.txt
```

b.

Dengan syarat yang dibutuhkan, maka pada script [register.sh](http://register.sh) menggunakan command:

```bash
${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && ! "$password" == "$username" ]];
```

Pengecekan username menggunakan fungsi grep yang membaca satu baris pada file user.txt dengan command:

```bash
! grep -q -s "$username" .users/user.txt;
```

Apabila username maupun password yang diinput tidak memenuhi syarat maka akan menampilkan massage yang dioutput pada file log.txt dengan command:

```bash
echo "$times REGISTER: ERROR User already exists" >> log.txt
echo "$times LOGIN: ERROR Failed login attempt on user $username" >> log.txt
```

Namun apabila user berhasil regis dan berhasil login maka massage yang dihasilkan dengan command:

```bash
echo "$times REGISTER: INFO User $username registered successfully" >> log.txt
echo "$times LOGIN: INFO User $username logged in" >> log.txt
```

Username dan password yang didaftarkan akan disimpan pada direktory users/user.txt dengan command:

```bash
echo "$username ; $password" >> .users/user.txt
```

1. Sistem login menggunakan fungsi AWK untuk mengecek username password apakah memenuhi syarat atau tidak dengan command:

```bash
ambil=$(awk -v user="${username}" -v pass="${password}" '$1==user && $3==pass {print 1}' .users/user.txt)
```

Kemudian menggunakan fungsi IF untuk menerima input ulang ketika user salah menginput username maupun password dengan command:

```bash
if [[ ! "$ambil" == 1 ]];then	echo "$times LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	read -p "username: " username
	read -p "password: " -s password
	ambil=$(awk -v user="${username}" -v pass="${password}" '$1==user && $2==pass {print 1}' .users/user.txt)fi
```

Ketika user berhasil login maka muncul massage dengan command:

```bash
echo "$times LOGIN: INFO User $username logged in" >> log.txt
```

Kemudian sebagai bentuk dokumentasi, ketika user berhasil login diberikan input command att atau dl N dengan N sebagai banyaknya gambar yang akan diunduh. Fungsi yang digunakan adalah fungsi IF yang dimana akan membaca input att atau dl N dengan command:

```bash
read -p "att / dl (N): " input
if [[ "$input" == "att" ]];
then
			awk -v user="${username}" '$3=="LOGIN:" && $6==user {++n} END {print "Login attempt = " n}' log.txt
```

Fungsi AWK digunakan untuk membaca percobaan login dari user dari file log.txt. Kemudian apabila input berupa dl N maka akan dihasilkan output berupa file zip yang berisi unduhan gambar dari website yang diberikan. Penamaan file nantinya sesuai dengan perintah pada soal.

```bash
sudo apt install zip unzip
	folder=`date +%F`"_$username"	index=0
	if [[ ! -f "./$folder.zip" ]]	then		mkdir -p "./$folder"	else		unzip -P "$password" "./$folder.zip"		file=`ls "$folder" -r | head -n 1`		index=$(awk -F"_"'{print $2}' <<< "$file")	fi	for ((num=index+1; num<=index+cc; num++))	do		wget -O "$folder/PIC_$num" https://loremflickr.com/320/240
	done	zip -r -P "$password" "$folder.zip" "$folder"	rm -rf "$folder"
```

- REVISI

Pada pengerjaan dan demo terdapat kendala yaitu fungsi AWK yang digunakan untuk mengambil nilai variabel username dan password tidak berfungsi dengan baik. Sehingga saya mencoba merubah fungsi untuk membuat file users/user.txt dan log.txt sebagai berikut:

```bash
home=/home/dimas/users
users_file=$home/user.txt
log=/home/dimas/log.txt

if [[ ! -d "$home" ]]
then
		mkdir $home
fi
```

# Soal Nomor 2

| Pengerjaan | NRP |
| --- | --- |
| Naftali Salsabila K | 5027201012 |
1. Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama “soal2_forensic_dapos.sh” untuk melaksanakan tugas-tugas berikut:
2. Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
3. Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
4. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
5. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
6. Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:

File ratarata.txt Rata-rata serangan adalah sebanyak rata_rata requests per jam

File result.txt IP yang paling banyak mengakses server adalah: ip_address sebanyak jumlah_request requests

Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent

IP Address Jam 2 pagi IP Address Jam 2 pagi dst

- Gunakan AWK ** Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log

## Solusi

`no 2a` Pertama kali yang dilakukan untuk keseluruhan kebutuhan masalah adalah membuat folder bernama `forensic_log_website_daffainfo_log` . Dan agar tidak terjadi error message saat pembuatan folder namun ternyata folder tersebut sudah terdapat dalam directory yang ditentukan maka menggunakan command `-p`

```
mkdir -p ./forensic_log_website_daffainfo_log
```

Setelah itu membuat file-file yang dibutuhkan yaitu `ratarata.txt` dan `result.txt` pada folder yang telah dibuat, dimana di sini file-filenya juga sama akan dibuat apabila belum ada dan apabila sudah terbuat maka file itu dicommand agar tidak langsung terbuka

```
test -f ./forensic_log_website_daffainfo_log/ratarata.txt || touch ./forensic_log_website_daffainfo_log/ratarata.txttest -f ./forensic_log_website_daffainfo_log/result.txt || touch ./forensic_log_website_daffainfo_log/result.txt
```

`no 2b` kemudian akan dicari rata-rata penyerangan tiap jamnya pada website `https://daffa.info` dengan membaca file `log_website_daffainfo.log`. setelah didapatkan data-datanya dan dilakukan perhitungan untuk mendapatkan angka rata-ratanya maka hasil dan message yang didapatkan akan dioutputkan pada file `ratarata.txt`  Di sini akan menggunakan awk dan pendeteksian tanda `:` sebagai pemisah antar argumen

```
avg(){msg="$1"avg_msg="$msg"echo "$avg_msg" >> forensic_log_website_daffainfo_log/ratarata.txt}cat forensic_log_website_daffainfo_log/log_website_daffainfo.log | awk -F ":" '{sum+=$3$4$5} END {rata_rata = sum/NR}'msg="Rata-rata serangan adalah sebanyak $rata_rata requests per jam"avg "$msg"
```

dimana hasil outputnya adalah sebagai berikut

![ouput no 2b](./img/ratarata.jpeg)

`no 2d` Kemudian pencarian request dengan user-agent curl, di sini akan dicari di setiap argumen dalam setiap line apakah ada string `curl`, dan apabila iya maka count akan terus bertambah dan disimpan pada variabel `jumlah_req_curl`

```
jumlah_req_curl=$(awk 'BEGIN {count=0} $NF ~ /curl/ {count++} END {print count}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
```

hasil dan message akan dioutputkan dalam file `result.txt`

```
echo "Ada $jumlah_req_curl requests yang menggunakan curl sebagai user-agent">> forensic_log_website_daffainfo_log/result.txt
```

Sebagai berikut

![ouput no 2d](./img/curl.jpeg)

`no 2e` Kemudian untuk menemukan daftar semua IP yang mengakses website pada pukul 2 pagi maka diperlukan menentukan banyaknya serangan pada tanggal tersebut

```
n=$(awk 'END{print NR}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
```

Kemudian dilakukan looping di setiap serangan tersebut dicek mana saja yang mengakses pada pukul 2 pagi, dimana di sini setiap argumen akan dipisahkan oleh tanda `:` dan untuk data waktu pengaksesan akan berada pada argumen ke-3, dan setiap data pengaksesan yang mengakses pada jam 2 maka akan didapatkan hasil berupa IP nya yang berada pada argumen ke-1 yang akan disimpan pada variabel `IP_Address_Jam_2_pagi`

```
for i in ndoIP_Address_Jam_2_pagi=$(awk -F ":" '$3==02 {print $1}' forensic_log_website_daffainfo_log/log_website_daffainfo.log)
```

Kemudian setiap hasil IP yang didapatkan di setiap looping akan dioutputkan pada file `result.txt`

```
echo "$IP_Address_Jam_2_pagi" >> forensic_log_website_daffainfo_log/result.txtdone
```

outputnya adalah sebagai berikut

![output 2e page 1](./img/IP1.jpeg) <br>
![output 2e page 2](./img/IP2.jpeg) <br>
![output 2e page 3](./img/IP3.jpeg) <br>

# Soal Nomor 3

| Pengerjaan | NRP |
| --- | --- |
| Ariel Aliski | 5027201058 |
1. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

1. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
2. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
3. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
4. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Note:

- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:

mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:

type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M

maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M

average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62

## Solusi

A dan B

Command yang ingin dioutput adalah

```bash
free -m
du -sh /home/kali
```

Sehingga buatlah file menggunakan **nano** untuk **minute_log.sh**

lalu dalam file tersebut masukkan

```bash
echo "$(free -m)" >> /home/kali/log/"metrics_$overwrite.log"
echo "$(du -sh /home/kali)" >> /home/kali/log/"metrics_$overwrite.log"
```

untuk mengoutput hasil command sebelumnya ke sebuah file bernama metrics_ + waktu berformat **YmdHs**.log

Sebab soal meminta perintah dioutputkan ke sebuah file dengan format YmdHs berikut maka saya mendeklarasikan sebuah variabel untuk menampung format tersebut yang nantinya setiap membuat file dicetak dengan metrics_(format file) sebagai berikut serta menyertakan directory foldernya

```bash
overwrite=$(date +'%Y%m%d%H%M%S')
touch /home/kali/log/"metrics_$overwrite.log"
```

Contoh output log adalah sebagai berikut : 

```bash
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```

Sehingga, saya harus menggunakan awk untuk mengambil bagian - bagian yang diinginkan saja, yaitu baris kedua dan ketiga pada bagian mem dan swap, serta untuk command du -sh <path>, diambil kolom-kolom sesuai instruksi soal

```bash
overwrite=$(date +'%Y%m%d%H%M%S')
touch /home/kali/log/"metrics_$overwrite.log"

while sleep 60

do

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_$overwrite.log"
mem=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ') 
sw=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')
path="/home/kali"
echo "$mem,$sw,$path/,$disk" >> /home/kali/log/"metrics_$overwrite.log"
chmod 600 /home/kali/log/"metrics_$overwrite.log"

done
```

Agar program bisa berjalan setiap 1 menit selama tak hingga, saya menggunakan infinite loop menggunakan while dengan sleep 60 (jalankan, menunggu 60 detik dan jalankan kembali)

```bash
overwrite=$(date +'%Y%m%d%H%M%S')
touch /home/kali/log/"metrics_$overwrite.log"

while sleep 60

do

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_$overwrite.log"
mem=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ') 
sw=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')
path="/home/kali"
echo "$mem,$sw,$path/,$disk" >> /home/kali/log/"metrics_$overwrite.log"
chmod 600 /home/kali/log/"metrics_$overwrite.log"

done
```

d.

Agar file tidak dapat diakses, maka gunakan set permissions pada linux, lalu pastikan untuk setting Group dan Others dibuat akses menjadi none

Sayangnya, perintah tersebut hanya memastikan terkuncinya folder, tetapi filenya sendiri tidak terkunci sehingga kita harus menambahkan chmod untuk mengatur permission file

```bash
chmod 600 /home/kali/log/"metrics_$overwrite.log"
```

Dan line ini dimasukkan sebelum loop diulang kembali, sehingga menjadi seperti berikut

```bash
overwrite=$(date +'%Y%m%d%H%M%S')
touch /home/kali/log/"metrics_$overwrite.log"

while sleep 60

do

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_$overwrite.log"
mem=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ') 
sw=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')
path="/home/kali"
echo "$mem,$sw,$path/,$disk" >> /home/kali/log/"metrics_$overwrite.log"
chmod 600 /home/kali/log/"metrics_$overwrite.log"

done
```

artinya setelah file digenerate akan di set permissionnya untuk user dengan value 6, group dengan value 0, dan user lain dengan value 0
(value 0 tidak memiliki akses dan value 6 memiliki akses RW)

Sebelumnya, saya mencoba untuk membuat akses dengan chmod 400, akan tetapi iterasi log menjadi gagal setelah loop pertama karena value 400 adalah *read only* sehingga setelah looping pertama, file tidak dapat diakses lagi

c. Setiap jam file tersebut diagregasi untuk mencari nilai minimum, maximum, dan rata rata untuk menghasilkan output sebagai berikut

```bash
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62
```

Untuk itu saya membuat sebuah file metrics_agg_{YmdH}.log ****sebagai berikut

```bash
time=$(date +'%Y%m%d%H')
touch /home/kali/log/"metrics_agg_$time.log"
chmod 600 /home/kali/log/"metrics_agg_$time.log"

logs=$(ls log/metrics_$time*)

for FILE in $logs
do
   cat $FILE | grep -v mem > tmp.txt
done

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_agg_$time.log"

awk '{if (min==""){min=max=$1};
	 if($1>max) {max=$1};
	  if($1<min) {min=$1};
	   total+=$1; count+=1}
	   END {print "minimum "min," \nmaximum "max," \naverage " total/count}' tmp.txt >> /home/kali/log/"metrics_agg_$time.log"
```

Saya mengambil file log yang mengandung pada satuan jam YmdH yang sesuai, dan memindahkan file tersebut ke sebuah file temporary untuk menghilangkan tulisan "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" agar lebih mudah untuk di awk

```bash
for FILE in $logs
do
   cat $FILE | grep -v mem > tmp.txt
done
```

Lalu yang saya awk adalah sebagai berikut

```bash
awk '{if (min==""){min=max=$1};
	 if($1>max) {max=$1};
	  if($1<min) {min=$1};
	   total+=$1; count+=1}
	   END {print "minimum "min," \nmaximum "max," \naverage " total/count}' tmp.txt >> /home/kali/log/"metrics_agg_$time.log"
```

Fungsi ini digunakan untuk mengambil kolom pertama dan membandingkannya dengan yang lain untuk menentukan value maximum dan minimumnya. Sayangnya, saya sudah mencoba untuk menambahkan total dan count untuk mencari average tetapi tidak berhasil teroutput
Saya juga sudah mencoba untuk mengakali dengan menggunakan (min+max)/2 namun tidak berhasil juga dan sama

```bash
time=$(date +'%Y%m%d%H')
touch /home/kali/log/"metrics_agg_$time.log"
chmod 600 /home/kali/log/"metrics_agg_$time.log"

logs=$(ls log/metrics_$time*)

for FILE in $logs
do
   cat $FILE | grep -v mem > tmp.txt
done

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_agg_$time.log"

awk '{if (min==""){min=max=$1};
	 if($1>max) {max=$1};
	  if($1<min) {min=$1};
	   total+=$1; count+=1}
	   END {print "minimum "min," \nmaximum "max," \naverage " total/count}' tmp.txt >> /home/kali/log/"metrics_agg_$time.log"
```

## Revisi Nomor 3

- 3b dan 3d

Sebab soal ternyata menyuruh untuk membuat file 1 log UNTUK 1 file, saya hanya cukup merubah loop pembuatan file menjadi didalam sehingga file terbuat 1 untuk setiap menitnya

Note - di gambar sleep 1 karena hanya untuk saya ujikan saja apakah filenya tergenerate otomatis atau tidak, seharusnya sleep 60 karena diulang tiap 1 menit

Source code aslinya seperti berikut

```bash
while sleep 1

do

overwrite=$(date +'%Y%m%d%H%M%S')
touch /home/kali/log/"metrics_$overwrite.log"

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/kali/log/"metrics_$overwrite.log"
mem=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ') 
sw=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')
path="/home/kali"
echo "$mem,$sw,$path/,$disk" >> /home/kali/log/"metrics_$overwrite.log"
chmod 400 /home/kali/log/"metrics_$overwrite.log"

done
```

Selain itu, karena sekarang 1 log file untuk 1 menit, saya bisa merubah chmod 600 (R&W) menjadi chmod 400 (R only) sebab file hanya perlu digenerate sekali dan tidak ditulis ulang

![Untitled](img/Untitled.png)

Untuk lebih menyempurnakan jawaban, saya akan menggunakan cron untuk mengulang perintah file sehingga tidak perlu menggunakan loop

Setup cron dilakukan sebagai berikut

![Untitled](img/cron.png)

Dua command diatas dijalankan untuk menginstall dan meng-enable cronjob

Lalu saya akan mengsetup cron sebagai berikut

![Untitled](img/cronjob.png)

Save dan cron akan otomatis terjalan setiap menit

3c

Sebab nomor 3 saya belum berhasil mengeluarkan nilai average, saya merubah cara pengerjaan sebagai berikut, penjelasan terletak didalam kode

```bash
cd /home/kali/log
time=$(date +'%Y%m%d%H')
tmp=tmp_$(date +\%Y\%m\%d\%H\%M\%S).log
#Import semua file log yang ada pada 1 jam terakhir, automasi dengan cron
cat metrics_2022* > $tmp
format=metrics_agg_$time.log
#Menghitung rerata dengan awk field separator, yang dibagi dengan jumlah seluruh item
awk -F, '{
mem_total += $1;mem_used += $2; mem_free += $3; mem_shared += $4;mem_buff += $5;mem_available += $6;
swap_total += $7;swap_used += $8;swap_free += $9;
path=$10
path_size+=$11
count++ } 
END { 
print "average "mem_total/count","mem_used/count","mem_free/count","mem_shared/count","mem_buff/count","mem_available/count","swap_total/count","swap_used/count","swap_free/count","path","path_size/count"M";
}' $tmp >> $format
#Menghitung minimum maximum, dengan sorting dan mengambil head
mem_total_min=$(awk -F, '{print $1}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_total_max=$(awk -F, '{print $1}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
mem_used_min=$(awk -F, '{print $2}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_used_max=$(awk -F, '{print $2}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
mem_free_min=$(awk -F, '{print $3}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_free_max=$(awk -F, '{print $3}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
mem_shared_min=$(awk -F, '{print $4}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_shared_max=$(awk -F, '{print $4}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
mem_buff_min=$(awk -F, '{print $5}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_buff_max=$(awk -F, '{print $5}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
mem_available_min=$(awk -F, '{print $6}' $tmp | sort -n | head -n 1 | awk '{print $1}') mem_available_max=$(awk -F, '{print $6}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
swap_total_min=$(awk -F, '{print $7}' $tmp | sort -n | head -n 1 | awk '{print $1}') swap_total_max=$(awk -F, '{print $7}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
swap_used_min=$(awk -F, '{print $8}' $tmp | sort -n | head -n 1 | awk '{print $1}') swap_used_max=$(awk -F, '{print $8}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
swap_free_min=$(awk -F, '{print $9}' $tmp | sort -n | head -n 1 | awk '{print $1}') swap_free_max=$(awk -F, '{print $9}' $tmp | sort -nr | head -n 1 | awk '{print $1}')
path=$(awk -F, '{print $10}' $tmp | head -n 1 | awk '{print $1}')
path=$(awk -F, '{print $10}' $tmp | head -n 1 | awk '{print $1}')
path_size_min=$(awk -F, '{print $11}' $tmp | sort -n | head -n 1 | awk '{print $1}') path_size_max=$(awk -F, '{print $11}' $tmp | sort -nr | head -n 1 | awk '{print $1}')

echo "minimum $mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$path,$path_size_min" >> $format
echo "maximum $mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$path,$path_size_max" >> $format
chmod 400 $format
rm "$tmp"
```

Kode diatas bekerja seperti berikut

- Average mengambil awk dengan field separator, yang akan menghitung jumlah data pada kolom ($1 ... $11) lalu nilai nilainya dihitung. Setelah itu nilainya akan dibagi dengan banyak jumlah data yang ada sehingga bisa mendapatkan nilai rerata
- Untuk nilai minimum maksimum, nilai minimum didapatkan dengan cara mensorting secara **descending** dan diambil headnya, sementara untuk nilai maksimum didapatkan dengan cara mengambil nilai secara **ascending** dan diambil headnya, lalu nilainya disimpan pada sebuah variabel

Output untuk nomor 3 adalah sebagai berikut

![Untitled](img/gambar4.png)